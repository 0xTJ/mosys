# Mosys Operating System

This is currently designed for use with [rosco_m68k](https://rosco-m68k.com/).

This project is unstable, and in early development. The kernel API will never be stable. The user API is not currently stable.

Submodules must be cloned.

For rosco_m68k, boot `ROSCODE1.BIN` via Kermit, or boot `ROSCODE1.BIN` or `ROSCODE1.ELF` from an SD card.

## Building

To build, use `cmake`, targetting the root of the repository, with the toolchain set to one in the `mosys-kernel/toolchains` directory (e.g. `mkdir build && cd build && cmake -DCMAKE_TOOLCHAIN_FILE=../mosys-kernel/toolchains/m68k-none.cmake .. && make`).

To increase build speed, pass the argument `-j<thread count>` to `make` (e.g. `make -j6`).
