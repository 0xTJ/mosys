cmake_minimum_required(VERSION 3.19)
project(Mosys)

include(ExternalProject)

set(MOSYS_ARCH "${CMAKE_SYSTEM_PROCESSOR}")
set(MOSYS_SUBARCH "${CMAKE_SYSTEM_NAME}")

ExternalProject_Add(c
    SOURCE_DIR "${CMAKE_SOURCE_DIR}/mosys-pdclib"
    CMAKE_ARGS
        "-DCMAKE_INSTALL_PREFIX:PATH=<INSTALL_DIR>"
    CMAKE_CACHE_DEFAULT_ARGS
        "-DCMAKE_TOOLCHAIN_FILE:FILEPATH=${CMAKE_SOURCE_DIR}/mosys-user/toolchains/${MOSYS_ARCH}-mosys.cmake"
        "-DCMAKE_EXPORT_COMPILE_COMMANDS:BOOL=${CMAKE_EXPORT_COMPILE_COMMANDS}"
    BUILD_ALWAYS true
)

ExternalProject_Get_property(c INSTALL_DIR)
set(BUILD_PREFIX "${INSTALL_DIR}")

ExternalProject_Add(user
    SOURCE_DIR "${CMAKE_SOURCE_DIR}/mosys-user"
    CMAKE_ARGS
        "-DCMAKE_INSTALL_PREFIX:PATH=<INSTALL_DIR>"
        "-DCMAKE_PREFIX_PATH:PATH=${BUILD_PREFIX}"
    CMAKE_CACHE_DEFAULT_ARGS
        "-DCMAKE_TOOLCHAIN_FILE:FILEPATH=${CMAKE_SOURCE_DIR}/mosys-user/toolchains/${MOSYS_ARCH}-mosys.cmake"
        "-DCMAKE_EXPORT_COMPILE_COMMANDS:BOOL=${CMAKE_EXPORT_COMPILE_COMMANDS}"
    INSTALL_COMMAND true
    DEPENDS c
    BUILD_ALWAYS true
)

ExternalProject_Get_property(user BINARY_DIR)
set(USER_BINARY_DIR "${BINARY_DIR}")
ExternalProject_Get_property(user INSTALL_DIR)
set(MOSYS_ROOT "${INSTALL_DIR}/root")

add_custom_target(initrd ALL
    COMMAND ${CMAKE_COMMAND} --install "${USER_BINARY_DIR}" --prefix "${CMAKE_CURRENT_BINARY_DIR}/initrd" --component initrd
    DEPENDS user
    )

add_custom_target(root ALL
    COMMAND ${CMAKE_COMMAND} --install "${USER_BINARY_DIR}" --prefix "${CMAKE_CURRENT_BINARY_DIR}/root" --component root
    DEPENDS user
    )

add_custom_command(OUTPUT "${CMAKE_CURRENT_BINARY_DIR}/initrd.cpio"
    COMMAND bash -c "find . \\| sed 's!^\\./!!' \\| sed 's!^\\.\\$$!!' \\| awk NF \\| cpio -D ${CMAKE_CURRENT_BINARY_DIR}/initrd -oc --quiet >${CMAKE_CURRENT_BINARY_DIR}/initrd.cpio"
    DEPENDS initrd
    WORKING_DIRECTORY "${CMAKE_CURRENT_BINARY_DIR}/initrd"
    )
add_custom_target(initrd_cpio ALL
    DEPENDS "${CMAKE_CURRENT_BINARY_DIR}/initrd.cpio"
    )

ExternalProject_Add(kernel
    SOURCE_DIR "${CMAKE_SOURCE_DIR}/mosys-kernel"
    CMAKE_ARGS
        "-DINITRD_CPIO:FILEPATH=${CMAKE_CURRENT_BINARY_DIR}/initrd.cpio"
    CMAKE_CACHE_DEFAULT_ARGS
        "-DCMAKE_TOOLCHAIN_FILE:STRING=${CMAKE_SOURCE_DIR}/mosys-kernel/toolchains/${MOSYS_ARCH}-none.cmake"
        "-DCMAKE_EXPORT_COMPILE_COMMANDS:BOOL=${CMAKE_EXPORT_COMPILE_COMMANDS}"
    STEP_TARGETS mosys.elf
    INSTALL_COMMAND true
    DEPENDS initrd_cpio
    BUILD_ALWAYS true
)

ExternalProject_Get_property(kernel BINARY_DIR)
set(KERNEL_BINARY_DIR "${BINARY_DIR}")

add_custom_command(
    OUTPUT ROSCODE1.ELF
    COMMAND ${CMAKE_COMMAND} -E copy "${KERNEL_BINARY_DIR}/mosys.elf" "${CMAKE_CURRENT_BINARY_DIR}/ROSCODE1.ELF"
    DEPENDS "${KERNEL_BINARY_DIR}/mosys.elf"
)

add_custom_command(
    OUTPUT ROSCODE1.BIN
    COMMAND ${CMAKE_OBJCOPY} ARGS -O binary "${KERNEL_BINARY_DIR}/mosys.elf" "${CMAKE_CURRENT_BINARY_DIR}/ROSCODE1.BIN"
    DEPENDS "${KERNEL_BINARY_DIR}/mosys.elf"
)

add_custom_target(
    rosco_m68k_files ALL
    DEPENDS ROSCODE1.ELF
    DEPENDS ROSCODE1.BIN
)
